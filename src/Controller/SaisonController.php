<?php

namespace App\Controller;

use App\Entity\Saison;
use App\Form\SaisonType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SaisonController extends AbstractController
{
    #[Route('/saison/nouveau', name: 'saison_nouveau')]
    public function nouveau(
        EntityManagerInterface $entityManager,
        Request $request
    ): Response
    {
        $saison = new Saison();
        $saisonForm = $this->createForm(SaisonType::class, $saison);
        $saisonForm->handleRequest($request);
        if ($saisonForm->isSubmitted() && $saisonForm->isValid()) {
            $entityManager->persist($saison);
            $entityManager->flush();
            return $this->redirectToRoute('mon_deuxieme_index');
        }
        return $this->render(
            'saison/nouveau.html.twig',
            compact('saisonForm')
        );
    }
}
