<?php

namespace App\Controller;

use App\Entity\Serie;
use App\Form\SerieType;
use App\Repository\SerieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/serie', name: 'mon_deuxieme')]
class MonDeuxiemeController extends AbstractController
{
    #[Route('/nouveau', name: '_nouveau')]
    #[IsGranted("ROLE_ADMIN")]
    public function nouveau(
        Request                $request,
        EntityManagerInterface $entityManager
    ): Response
    {
        // 1 : Créer une instance de Serie
        $serie = new Serie();
        // 2 : Créer une instance du formulaire
        $serieForm = $this->createForm(SerieType::class, $serie);
        // 3 : Envoyer le formulaire au twig
        $serieForm->handleRequest($request);
        if ($serieForm->isSubmitted() && $serieForm->isValid()) {
            $entityManager->persist($serie);
            $entityManager->flush();
            return $this->redirectToRoute('mon_deuxieme_index');
        }

        return $this->render(
            'mon_deuxieme/nouveau.html.twig',
            compact('serieForm')
        );
    }


    #[Route('/', name: '_index')]
    public function index(
        SerieRepository $serieRepository
    ): Response
    {
        // Toutes les séries en base de données
        $series = $serieRepository->findAll(); // SELECT ALL
        // Les dix premières séries dont la date est 2002 triée par ordre alphabétique de titre
        $series = $serieRepository->findBy(
            [], // WHERE
            ["titre" => "ASC"], // ORDER BY
            10, // LIMIT
            0 // OFFSET
        );
        return $this->render('mon_deuxieme/index.html.twig',
            compact('series')
        );
    }

    #[Route(
        '/{id}',
        name: '_une_seule_serie',
        requirements: ['id' => '\d+']
    )]
    public function une_seule_serie(
        int             $id,
        SerieRepository $serieRepository
    ): Response
    {
        $serie = $serieRepository->findOneBy(['id' => $id]);
        return $this->render(
            'mon_deuxieme/une_seule_serie.html.twig',
            compact('serie')
        );
    }

}
