<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MonPremierController extends AbstractController
{
    #[Route('/mon/premier/index1', name: 'app_mon_premier')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/MonPremierController.php',
        ]);
    }

    #[Route('/mon/premier/index2', name: 'app_mon_premier')]
    public function index2(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/MonPremierController.php',
        ]);
    }
}
