<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LeFaitesPasVousController extends AbstractController
{
    #[Route('/le/faites/pas/vous', name: 'app_le_faites_pas_vous')]
    public function index(): Response
    {
        return $this->render('le_faites_pas_vous/index.html.twig', [
            'controller_name' => 'LeFaitesPasVousController',
        ]);
    }
}
